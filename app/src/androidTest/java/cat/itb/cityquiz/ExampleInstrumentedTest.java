package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.StartQuiz;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<StartQuiz> mActivityTestRule = new ActivityTestRule<>(StartQuiz.class);

    @Test
    public void navigationTest() {

        onView(withId(R.id.startQuiz_btn))
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.imageQuestion))
                .check(matches(isDisplayed()));

        onView(withId(R.id.city1))
                .check(matches(isClickable()));
        onView(withId(R.id.city2))
                .check(matches(isClickable()));
        onView(withId(R.id.city3))
                .check(matches(isClickable()));
        onView(withId(R.id.city4))
                .check(matches(isClickable()));
        onView(withId(R.id.city5))
                .check(matches(isClickable()));
        onView(withId(R.id.city6))
                .check(matches(isClickable()));

        onView(withId(R.id.city1))
                .perform(click());


        onView(withId(R.id.city1))
                .check(matches(isClickable()));
        onView(withId(R.id.city2))
                .check(matches(isClickable()));
        onView(withId(R.id.city3))
                .check(matches(isClickable()));
        onView(withId(R.id.city4))
                .check(matches(isClickable()));
        onView(withId(R.id.city5))
                .check(matches(isClickable()));
        onView(withId(R.id.city6))
                .check(matches(isClickable()));

        onView(withId(R.id.city2))
                .perform(click());

        onView(withId(R.id.city1))
                .check(matches(isClickable()));
        onView(withId(R.id.city2))
                .check(matches(isClickable()));
        onView(withId(R.id.city3))
                .check(matches(isClickable()));
        onView(withId(R.id.city4))
                .check(matches(isClickable()));
        onView(withId(R.id.city5))
                .check(matches(isClickable()));
        onView(withId(R.id.city6))
                .check(matches(isClickable()));

        onView(withId(R.id.city3))
                .perform(click());

        onView(withId(R.id.city1))
                .check(matches(isClickable()));
        onView(withId(R.id.city2))
                .check(matches(isClickable()));
        onView(withId(R.id.city3))
                .check(matches(isClickable()));
        onView(withId(R.id.city4))
                .check(matches(isClickable()));
        onView(withId(R.id.city5))
                .check(matches(isClickable()));
        onView(withId(R.id.city6))
                .check(matches(isClickable()));

        onView(withId(R.id.city4))
                .perform(click());

        onView(withId(R.id.city1))
                .check(matches(isClickable()));
        onView(withId(R.id.city2))
                .check(matches(isClickable()));
        onView(withId(R.id.city3))
                .check(matches(isClickable()));
        onView(withId(R.id.city4))
                .check(matches(isClickable()));
        onView(withId(R.id.city5))
                .check(matches(isClickable()));
        onView(withId(R.id.city6))
                .check(matches(isClickable()));

        onView(withId(R.id.city5))
                .perform(click());



        onView(withId(R.id.messageScore))
                .check(matches(isDisplayed()))
                .check(matches(withText("Your score is")));


        onView(withId(R.id.score))
                .check(matches(isDisplayed()));

        onView(withId(R.id.playAgain_btn))
                .perform(click());
    }
}
