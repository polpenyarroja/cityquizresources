package cat.itb.cityquiz.presentation.screens.startquiz;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.QuizViewModel;

public class StartQuizFragment extends Fragment {

    private QuizViewModel quizViewModel;

    public static StartQuizFragment newInstance() {
        return new StartQuizFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        quizViewModel.getGame().observe(this, this::onGameChanged);

        Button button=getView().findViewById(R.id.startQuiz_btn);
        button.setOnClickListener(this::toQuestionScreen);
    }

    private void onGameChanged(Game game) {
        if(game!=null){
            Navigation.findNavController(getView()).navigate(R.id.action_startQuizFragment_to_questionFragment);
        }
    }

    private void toQuestionScreen(View view) {
        quizViewModel.startQuiz();
    }

}
