package cat.itb.cityquiz.presentation.screens.question;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.QuizViewModel;
import cat.itb.cityquiz.presentation.screens.result.ScoreFragmentDirections;

public class QuestionFragment extends Fragment {

    private QuizViewModel quizViewModel;
    ImageView imageQuestion;
    TextView city1;
    TextView city2;
    TextView city3;
    TextView city4;
    TextView city5;
    TextView city6;
    Game game;


    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageQuestion = getView().findViewById(R.id.imageQuestion);
        city1 = getView().findViewById(R.id.city1);
        city2 = getView().findViewById(R.id.city2);
        city3 = getView().findViewById(R.id.city3);
        city4 = getView().findViewById(R.id.city4);
        city5 = getView().findViewById(R.id.city5);
        city6 = getView().findViewById(R.id.city6);

        city1.setOnClickListener(this::buttonClicked);
        city2.setOnClickListener(this::buttonClicked);
        city3.setOnClickListener(this::buttonClicked);
        city4.setOnClickListener(this::buttonClicked);
        city5.setOnClickListener(this::buttonClicked);
        city6.setOnClickListener(this::buttonClicked);


    }

    private void buttonClicked(View view) {
        if(view==city1){
            questionAnsered(0);
        }
        if(view==city2){
            questionAnsered(1);
        }
        if(view==city3){
            questionAnsered(2);
        }
        if(view==city4){
            questionAnsered(3);
        }
        if(view==city5){
            questionAnsered(4);
        }
        if(view==city6) {
            questionAnsered(5);
        }

    }

    private void questionAnsered(int i) {
        quizViewModel.answerQuestion(i);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);

        quizViewModel.getGame().observe(this,this::onGameChanged);





    }

    private void onGameChanged(Game game) {

        if(!game.isFinished()){
            display(game);
        }else{
            Navigation.findNavController(getView()).navigate(R.id.toScore);
        }

    }


    private void display(Game game) {
        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageQuestion.setImageResource(resId);

        List<City> answers= game.getCurrentQuestion().getPossibleCities();
        city1.setText(answers.get(0).getName());
        city2.setText(answers.get(1).getName());
        city3.setText(answers.get(2).getName());
        city4.setText(answers.get(3).getName());
        city5.setText(answers.get(4).getName());
        city6.setText(answers.get(5).getName());



    }



}
