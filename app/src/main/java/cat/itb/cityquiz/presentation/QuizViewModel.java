package cat.itb.cityquiz.presentation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {

    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game>  game = new MutableLiveData<>();

    public void startQuiz() {
        Game newGame = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        game.postValue(newGame);
    }

    public MutableLiveData<Game> getGame() {
        return game;
    }

    public void answerQuestion(int response){
        Game newGame = gameLogic.answerQuestions(game.getValue(), response);
        game.postValue(newGame);
    }




}
