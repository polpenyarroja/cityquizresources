package cat.itb.cityquiz.presentation.screens.result;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.QuizViewModel;

public class ScoreFragment extends Fragment {

    private QuizViewModel quizViewModel;
    TextView score;


    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        quizViewModel.getGame().observe(this,this::onGameChanged);

        Button button = getView().findViewById(R.id.playAgain_btn);
        button.setOnClickListener(this::backToStart);


    }

    private void onGameChanged(Game game) {
        if(game.isFinished()) {
            score = getView().findViewById(R.id.score);
            int scorePoints = game.getNumCorrectAnswers();
            score.setText(scorePoints + "");
        } else {
            Navigation.findNavController(getView()).navigate(R.id.action_scoreFragment_to_questionFragment);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void backToStart(View view) {

        quizViewModel.startQuiz();
    }

}
